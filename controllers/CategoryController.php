<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 26.12.2017
 * Time: 19:09
 */

namespace app\controllers;
use app\controllers\CustomController;
use app\models\Category;
use app\models\Product;
use app\models\Size;
use app\modules\admin\models\ValueProducr;
use Yii;
use yii\data\ActiveDataProvider;

class CategoryController extends CustomController
{

    /**
     * @return string
     * Вывод продукта по выбраной категории
     */
    public function actionView()
    {

        $id = Yii::$app->request->get('id');

        //$category = Category::findOne(['id' => end($id)]);
        $category = Category::findOne(['id' => $id]);

        $ids = [];// Массив id категории

        $this->setMeta('Магазин | ' .$category->title, $category->description, $category->keywords);

        // Если выбрана родительская категория, то быбераем  дочернии категории
        if ($category->parentId == 0){
            foreach ($category->children as $idChildren){
                $ids[] = $idChildren->id;
            }
        }else {
            //$ids[] = end($id);
            $ids[] = $id;
        }

        // Выборка продукта по категории
        $query = Product::find()->where(['categoryId' => $ids]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6
            ]
        ]);

        return $this->render('view' , compact('dataProvider'));


    }

    /**
     * @return string
     * Делаем выборку по дополнительным параметрам и сортировку
     */
    public function actionPodbor()
    {
        $idCategory = Yii::$app->request->get('id'); // id категории
        $dopParamsProducts = Yii::$app->request->get('DopParamsProduct'); // параметры запроса
        $range = ''; // диапозон цены
        $min = ''; // Минимальная цена
        $max = ''; // Максимальная цена
        $sort = ''; // Сортировка
        $propertyProduct = array(); // масив выбранных свойств
        foreach ($dopParamsProducts as $key => $value){

            // Из многомерного массива в одномерный
            if($key == 'propertyValue'){
                //array_delete($value);
                foreach ($value as $k =>$v){
                    if($v == ' ' || $v == '')
                        unset($value[$k]);
                }

                if($value != null){
                    $propertyProduct = call_user_func_array('array_merge', $value);
                }

            }

            // приводим к переменым мин и макс цена
            else if($key == 'range'){
                $range = explode('-',$value);
                $min = min($range);
                $max = max($range);
            }

            else if ($key == 'hid'){

                $sort = $value;
            }

        }

        //Вытоскиваем все idProduct у которого есть свойства
        if($propertyProduct != null){
            $valueProcuctr = ValueProducr::find()->where(['idValue' => $propertyProduct])->all();
            $idValueProduct = array();

            foreach ($valueProcuctr as $idProduct){
                $idValueProduct[] = $idProduct->idProduct;
            }
            $idValueProduct = array_unique($idValueProduct);
        }




        //Выборка по категориям

        $category = Category::findOne(['id' => $idCategory]);

        $ids = array();

        $this->setMeta('Магазин | ' .$category->title, $category->description, $category->keywords);

        if ($category->parentId == 0){
            foreach ($category->children as $idChildren){
                $ids[] = $idChildren->id;
            }
        }else {
            //$ids[] = end($id);
            $ids[] = $idCategory;
        }


        // Выбераем продокты с категорией
        $query = Product::find()->where(['categoryId' => $ids]);

        // Выборка по id продукта по сохранённым свойств
        if ($idValueProduct != null){
            $query->andWhere(['id' => $idValueProduct]);
        }

        // Выборка по цене от до
        $query->andWhere(['between', 'price', $min, $max]);


        // Сортировка
        switch ($sort){
            case 1 :
                $query->orderBy(['hit' => SORT_DESC]);
                break;

            case 2 :
                $query->orderBy(['sale' => SORT_DESC]);
                break;

            case 3 :
                $query->orderBy(['price' => SORT_ASC]);
                break;

            case -3 :
                $query->orderBy(['price' => SORT_DESC]);
                break;
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6
            ]
        ]);

        return $this->render('view' , compact('dataProvider'));
    }
}