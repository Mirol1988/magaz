-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 28 2018 г., 21:11
-- Версия сервера: 10.1.28-MariaDB
-- Версия PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `magaz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1514107904),
('manager', '6', 1514107904);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1514107904, 1514107904),
('AdminPage', 2, 'Админки', NULL, NULL, 1514107904, 1514107904),
('courier', 1, NULL, NULL, NULL, 1514107904, 1514107904),
('manager', 1, NULL, NULL, NULL, 1514107904, 1514107904),
('user', 1, NULL, NULL, NULL, 1514107904, 1514107904),
('viewPanelCourier', 2, 'Панель курьера', NULL, NULL, 1514107904, 1514107904),
('viewPanelManager', 2, 'Панель менеджера', NULL, NULL, 1514107904, 1514107904);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'AdminPage'),
('admin', 'manager'),
('courier', 'viewPanelCourier'),
('manager', 'courier'),
('manager', 'viewPanelManager');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parentId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `description` varchar(255) NOT NULL COMMENT 'Описание',
  `keywords` text NOT NULL COMMENT 'Ключевые слова',
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `parentId`, `title`, `description`, `keywords`, `url`) VALUES
(1, 0, 'Для мальчиков', 'Мальчики', 'Мальчики', 'dlya-malchikov'),
(2, 1, 'Кроссовки', 'Кроссовки', 'Кроссовки', 'krossovki'),
(3, 1, 'Ботинки', 'Ботинки', 'Ботинки', 'botinki'),
(4, 0, 'Для девочек', 'Для девочек', 'Для девочек', 'dlya-devochek'),
(5, 4, 'Кроссовки', 'Кроссовки', 'Кроссовки', 'krossovki'),
(6, 4, 'Туфли', 'Туфли', 'Туфли', 'tufli'),
(7, 0, 'Женщины', 'Женщины', 'Женщины', 'zhenshiny'),
(8, 0, 'Мужчины', 'Мужчины', 'Мужчины', 'muzhchiny');

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `isMain` tinyint(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(1, 'Products/Product2/092e1d.png', 2, 1, 'Product', '61ac5831fb-1', '');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1514106345),
('m140506_102106_rbac_init', 1514106408),
('m140622_111540_create_image_table', 1516134982),
('m140622_111545_add_name_to_image_table', 1516134982),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1514106408);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salePrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `hit` enum('0','1') DEFAULT NULL,
  `new` enum('0','1') DEFAULT NULL,
  `sale` enum('0','1') DEFAULT NULL,
  `view` int(11) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `categoryId`, `title`, `content`, `price`, `salePrice`, `keywords`, `description`, `hit`, `new`, `sale`, `view`, `url`) VALUES
(2, 2, '1', '1', '1.00', '1.00', '1', '1', '0', '0', '1', 0, '1'),
(3, 6, 'Классика', 'Классика', '2000.00', '0.00', 'Классика', 'Классика', '1', '0', '0', 0, 'klassika'),
(4, 2, 'Классика', 'Классика', '2000.00', '0.00', 'Классика', 'Классика', '1', '0', '0', 0, 'klassika'),
(5, 1, 'slkdnvkl', 'slkdnvkjn', '1.00', '0.00', '1', '1', '0', '0', '0', 0, 'slkdnvkl'),
(6, 1, 'slkdnvkl', 'slkdnvkjn', '1.00', '0.00', '1', '1', '0', '0', '0', 0, 'slkdnvkl'),
(7, 1, '234', '234', '1.00', '0.00', '32', '23', '0', '0', '0', 234, '234'),
(8, 1, '234', '234', '1.00', '0.00', '32', '23', '0', '0', '0', 234, '234'),
(9, 1, '234', '234', '1.00', '0.00', '32', '23', '0', '0', '0', 234, '234'),
(10, 1, '234', '234', '1.00', '0.00', '32', '23', '0', '0', '0', 234, '234'),
(11, 1, 'lskdbkjds', 'silken', '1.00', '1.00', '1', '1', '0', '0', '0', 1, 'lskdbkjds'),
(12, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(13, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(14, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(15, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(16, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(17, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(18, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(19, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(20, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(21, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(22, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd'),
(23, 1, 'dcdd', 'dcda', '1.00', '0.00', '1', '1', '0', '0', '0', 1, 'dcdd');

-- --------------------------------------------------------

--
-- Структура таблицы `property`
--

CREATE TABLE `property` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property`
--

INSERT INTO `property` (`id`, `name`) VALUES
(1, 'Цвет'),
(2, 'Размер'),
(3, 'Диаметр');

-- --------------------------------------------------------

--
-- Структура таблицы `property_values`
--

CREATE TABLE `property_values` (
  `id` int(11) NOT NULL,
  `idProperty` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property_values`
--

INSERT INTO `property_values` (`id`, `idProperty`, `value`) VALUES
(1, 1, 'Коричневый'),
(2, 1, 'Синий'),
(3, 1, 'Оранжевый'),
(4, 2, '23'),
(5, 2, '40'),
(6, 2, '36'),
(7, 3, '2.5cm');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `auth_key` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_email` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `active`, `auth_key`, `code`, `is_email`) VALUES
(1, 'Mirol1988@gmail.com', '', '$2y$13$K5g5XxakuLlvDESFpUQUbuOdfg/4EtvRSt3NwVZDehtSFRwu6Mtdy', 1, '96E6A3fDy4g45Utw6G9dS8916_wVKNp1', '0WDlVk6WGJ', 1),
(6, 'Mirol1988@icloud.com', '', '$2y$13$J3Hyi4DrUXHMQx2Vy/NsS.kL0kH3z4j6W6zUidF2yI/T2tDYUAAbS', 1, '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `value_producr`
--

CREATE TABLE `value_producr` (
  `id` int(11) NOT NULL,
  `idValue` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `value_producr`
--

INSERT INTO `value_producr` (`id`, `idValue`, `idProduct`) VALUES
(28, 1, 4),
(29, 2, 4),
(30, 4, 4),
(31, 5, 4),
(35, 3, 6),
(36, 4, 6),
(41, 1, 23),
(42, 2, 23),
(43, 4, 23),
(44, 7, 23);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `property_values`
--
ALTER TABLE `property_values`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `value_producr`
--
ALTER TABLE `value_producr`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `property`
--
ALTER TABLE `property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `property_values`
--
ALTER TABLE `property_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `value_producr`
--
ALTER TABLE `value_producr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
