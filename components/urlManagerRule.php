<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 27.12.2017
 * Time: 11:04
 */

namespace app\components;

use app\controllers\CustomController;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;

class urlManagerRule extends BaseObject implements UrlRuleInterface
{


    //Формирует ссылки в заданном виде
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'category/view')
        {
           $model = Category::find()->where(['id' => $params['id']])->with('parent')->one();

           if($model->parent){
               $url = 'category/' . $model->parent->url . '/' . $model->url;
           } else {
               $url = 'category/' . $model->url;
           }

           if(count($params ) > 1)
           {
               foreach ($params as $key => $value)
               {
                   if($key != 'id'){
                       $url = $url . '?' . $key . '=' . $value;
                   }
               }
           }

           return $url;

        }

        return false;
    }

    //Разбирает входящий URL запрос, преобразует ссылки произвольного вида  в нужный для Yii2
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();


        $urls = explode("/", $pathInfo);



        $params = $request->absoluteUrl;

        $param = explode("?", $params);

        //$dopParam = explode("&", $param[1]);

        //CustomController::printr($param);

        unset($param[0]);

        //CustomController::printr($param);




        if ($urls[0] == 'category')
        {
                unset($urls[0]);

                $id = [];

                if (count($urls) > 1) {
                    $cat = Category::find()->where(['url' => reset($urls)])->one();
                    $model = Category::find()->where(['url' => end($urls), 'parentId' => $cat->id])->asArray()->one();
                } else {
                    $model = Category::find()->where(['url' => $urls])->asArray()->one();
                }

                $id['id'][] = $model['id'];


            $params = array();
            //если есть параметры - вставляем их
            if(isset($param)){
                $temp = explode('&',$param[1]);
                //CustomController::printr($temp);
                foreach($temp as $t){
                    $t = explode('=', $t);
                    $params[$t[0]] = $t[1];
                }



               // CustomController::printr($params);
            }

                /*$params = array();
                //если есть параметры - вставляем их
                if(isset($link_data[1])){
                    $temp = explode('&',$urls[1]);
                    foreach($temp as $t){
                        $t = explode('=', $t);
                        $params[$t[0]] = $t[1];
                    }
                }*/

                return ['category/view' , $id, $params];
        }

        // Возврощаем норммальный url для админки
        else if ($urls[0] == 'admin')
        {
            $pathInfoAdmin = $request->url;

            $urls = explode("=", $pathInfoAdmin);

            $id['id'] = array_pop($urls);

            return [$pathInfo, $id];

        }


        return false;


    }
}
