<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $categoryId
 * @property string $title
 * @property string $content
 * @property string $price
 * @property string $salePrice
 * @property string $keywords
 * @property string $description
 * @property string $hit
 * @property string $new
 * @property string $sale
 * @property int $view
 * @property string $url
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'title', 'content', 'keywords', 'description', 'view', 'url'], 'required'],
            [['categoryId', 'view'], 'integer'],
            [['content', 'hit', 'new', 'sale'], 'string'],
            [['price', 'salePrice'], 'number'],
            [['title', 'keywords', 'description', 'url'], 'string', 'max' => 255],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryId' => 'Category ID',
            'title' => 'Title',
            'content' => 'Content',
            'price' => 'Price',
            'salePrice' => 'Sale Price',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'hit' => 'Hit',
            'new' => 'New',
            'sale' => 'Sale',
            'view' => 'View',
            'url' => 'Url',
        ];
    }
}
