<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 14.12.2017
 * Time: 0:17
 */

namespace app\models;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public static function tableName()
    {
        return 'category';
    }

    // Родитель
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parentId']);
    }

    // Дети
    public function getChildren()
    {
        return $this->hasMany(Category::className(), ['parentId' => 'id']);
    }

    public function getProduct()
    {
        return $this->hasMany(Product::className(), ['categoryId' => 'id']);
    }


    public function rules()
    {
        return [
            [['title', 'description', 'keywords', 'parentId'], 'required'],
            [['title', 'description', 'keywords', 'url'], 'string', 'max' => 255],
            [['parentId'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
          'title' => 'Загаловок',
          'description' => 'Описание',
          'keywords' => 'Ключивые слова',
          'parantId' => 'Категория',
        ];
    }
}