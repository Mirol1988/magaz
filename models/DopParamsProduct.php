<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 29.03.2018
 * Time: 22:20
 */

namespace app\models;
use yii\base\Model;


class DopParamsProduct extends Model
{
    public $propertyValue = array(); //id значение свойств
    public $range; //Для ползунка от до
    public $sort; // Сортировка выбора
    public $hid;


    const HIT = 1; //Хит
    const NO_HIT = -1; // не хит
    const SALE = 2; // скидка
    const NO_SALE = -2; // не скидка
    const PRISE = 3; // дешевли
    const NO_PRISE = -3; // дороже

    public function rules()
    {
        return [
            ['propertyValue', function($attribute, $params, $validator){
                if(!is_array($this->$attribute)){
                    $this->addError($attribute, 'Ой что то пошло не так');
                }
            }],
            [['range', 'sort', 'hid'], 'safe'],
            ['sort' , 'integer'],
        ];
    }

    /**
     * @return array
     * Возвращаем значение констант
     */
    public static function getTypes()
    {
        return [
            self::HIT    =>  'Самые популярные',
            self::SALE    =>  'Распродажа',
            self::PRISE      =>  'Цена дешевле',
            self::NO_PRISE      =>  'Цена дороже',
        ];
    }
}