<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CategoryMenu;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Product */
/* @var $form yii\widgets\ActiveForm */

$array = array();

$name = array();

$napoln = array();


//Создаём чекбокс лист
foreach ($property as $value){
    $array[$value->property->id][$value->id] = $value->value;
    $name[$value->property->id] = $value->property->name;


}

// Наполняем чекбокс лист
if (!$model->isNewRecord){
    foreach ($model->valueProduct as $prop){
        $napoln[$prop->property->id][] = $prop->id;
    }

    $model->propertyValue = $napoln;
}
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //$form->field($model, 'categoryId')->textInput() ?>

    <div class="from-group field-product-categoryId has-success">
        <label class="control-label" for="product-categoryId">Выберете категорию</label>
        <select id="product-categoryId" class="form-control" name="Product[categoryId]" aria-invalid="false">
           <?= CategoryMenu::widget(['view' => 'select_product', 'model' => $model]) ?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' =>'image/*']) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'salePrice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hit')->dropDownList([ '0' => 'Не хит', '1' => 'Хит', ], ['prompt' => 'Выберите статус']) ?>

    <?= $form->field($model, 'new')->dropDownList([ '0' => 'Не новая коллекция', '1' => 'Новая колекция', ], ['prompt' => 'Выберите статус']) ?>

    <?= $form->field($model, 'sale')->dropDownList([ '0' => 'Полная цена', '1' => 'Распродажа', ], ['prompt' => 'Выберите статус']) ?>

    <?= $form->field($model, 'view')->textInput() ?>

    <?php $gal = $model->getImages();//var_dump($gal)?>

    <?php if (!$model->isNewRecord && $model->getImages() != null) : ?>
        <div class="row">

            <?php  foreach ($gal as $file) :?>

                <div class="col-sm-4 col-md-2 col-lg-2 del_<?=$file->id?>">
                    <a href="#" class="thumbnail">
                        <?= Html::img("{$file->getUrl('150x150')}")?>
                        <span><i class="fa fa-times-circle del fa-lg delFoto glyphicon glyphicon-trash" data-id="<?= $model->id ?>" data-img = "<?= $file->id ?>"></i></span>
                    </a>
                </div>

            <?php endforeach; ?>
        </div>
    <?php endif; ?>


    <?php foreach ($array as $key => $value) :?>
        <?= $form->field($model, "propertyValue[$key]")
        ->checkboxList($value)
        ->label($name[$key]); ?>
    <?php  endforeach; ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
