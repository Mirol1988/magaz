<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Product */





$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редоктировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить продукт "'.$model->title.'"?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
              'attribute' => 'images',
              'value' => function($data)
              {
                  $gal = $data->getImages();
                  $str = '';
                  foreach ($gal as $file)
                  {
                      $str = $str . "<a href='{$file->getUrl('300x300')}' rel='galery' class='photo' >
                          <img src ='{$file->getUrl('100x100')}'> </a>";
                  }
                  return $str;
              } ,
                'format' => 'html',
            ],
            'categoryId',
            'title',
            'content:ntext',
            'price',
            'salePrice',
            'keywords',
            'description',

            [
                'attribute' => 'hit',
                'value' => function($data)
                {
                    switch ($data->hit)
                    {
                        case 1 :
                            return 'Да';
                            break;
                        case 0 :
                            return 'Нет';
                            break;
                    }
                },
            ],

            [
                'attribute' => 'new',
                'value' => function($data)
                {
                    switch ($data->new)
                    {
                        case 1 :
                            return 'Да';
                            break;
                        case 0 :
                            return 'Нет';
                            break;
                    }
                },
            ],
            [
                'attribute' => 'sale',
                'value' => function($data)
                {
                    switch ($data->sale)
                    {
                        case 1 :
                            return 'Да';
                            break;
                        case 0 :
                            return 'Нет';
                            break;
                    }
                },
            ],

            'view',
            'url:url',
        ],
    ]) ?>

</div>
