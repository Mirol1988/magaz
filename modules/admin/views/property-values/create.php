<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\PropertyValues */

$this->title = 'Создать значения свойств';
$this->params['breadcrumbs'][] = ['label' => 'Значение свойств', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-values-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'property' => $property,
    ]) ?>

</div>
