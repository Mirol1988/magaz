<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\PropertyValues */

$this->title = $model->property->name .' '. $model->value;
$this->params['breadcrumbs'][] = ['label' => 'Значение свойств', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-values-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редоктировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить значение свойства?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'idProperty',
                'value' => function ($data){
                    return $data->property->name;
                },
                'format' => 'html',
            ],
            'value',
        ],
    ]) ?>

</div>
