<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Значение свойств';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-values-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать свойство', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'idProperty',
                'value' => function ($data){
                    return $data->property->name;
                },
                'format' => 'html',
            ],

            'value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
