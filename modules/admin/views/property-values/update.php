<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\PropertyValues */

$this->title = 'Редоктировать значение свойств: ' .$model->property->name . ' ' .$model->value;
$this->params['breadcrumbs'][] = ['label' => 'Значение свойств', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->property->name . ' ' .$model->value, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редоктирование';
?>
<div class="property-values-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'property' => $property,
    ]) ?>

</div>
