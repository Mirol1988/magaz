<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $categoryId
 * @property string $title
 * @property string $content
 * @property string $price
 * @property string $salePrice
 * @property string $keywords
 * @property string $description
 * @property string $hit
 * @property string $new
 * @property string $sale
 * @property int $view
 * @property string $url
 */
class Product extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $propertyValue = array(); //id значение свойств

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return $this
     * Получаем свойства через связаную модель
     */
    public function getValueProduct()
    {
        return $this->hasMany(PropertyValues::className(), ['id' => 'idValue'])
            ->viaTable('value_producr' , ['idProduct' => 'id']);

    }

    public $images; // Галерея

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'title', 'content', 'keywords', 'description', 'view', 'url'], 'required'],
            [['categoryId', 'view'], 'integer'],
            [['content', 'hit', 'new', 'sale'], 'string'],
            [['price', 'salePrice'], 'number'],
            [['title', 'keywords', 'description', 'url'], 'string', 'max' => 255],
            [['images'], 'file', 'extensions' => 'png, jpg, jpeg', 'skipOnEmpty' => true, 'maxFiles' => 6],
            ['propertyValue', function($attribute, $params, $validator){
                if(!is_array($this->$attribute)){
                    $this->addError($attribute, 'Ой что то пошло не так');
                }
            }],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'categoryId' => 'Категория',
            'title' => 'Название',
            'content' => 'Описание',
            'price' => 'Цена',
            'salePrice' => 'Скидка',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'hit' => 'Хит',
            'new' => 'Новый',
            'sale' => 'Распродажа',
            'view' => 'Кол-во просмотров',
            'url' => 'Url адрес',
            'images' => 'Галерея',
        ];
    }

    /**
     * @return bool
     * Сохранение галлереи
     */
    public function uploadGallery()
    {
        if ($this->validate())
        {
            foreach ($this->images as $file)
            {
                $path = 'images/store' . $file->baseName .'.' . $file->extension;
                $file -> saveAs($path);
                $this->attachImage($path, false);
                @unlink($path);
            }
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * @param $array
     * @param $idProduct
     * Привязка значений к продукту
     */
    public function saveValue($array , $idProduct ){

        ValueProducr::deleteAll(['idProduct' => $idProduct]);
        foreach ($array as $value){
            if(!empty($value)){
                foreach ($value as $idValue){
                    $model= new ValueProducr();
                    $model->idProduct = $idProduct;
                    $model->idValue = $idValue;
                    $model->save();
                }
            }

        }
    }

}
